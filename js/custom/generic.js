/*
Generic classes and customizations to extjs
Copyright (C) 2008-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function storeException(proxy, type, action, options, response, arg) {
	var msg;
	var title;


	// Server did not return HTTP 200 result
	if (type == 'response') {
		title = 'Transport Error: ';

		// Check if we have server status
		if (response.status) {
			title = 'Server Error: ';
		}
		msg = response.statusText + ' (' + response.status + ')' ;


	// Server replied with HTTP 200, but successProperty is false
	} else if (type == 'remote') {
		title = 'Server Error: ';

		if (!isset(response.result)) {
			msg = 'No result returned';

		// Check success property is defined
		} else if (!isset(response.success)) {
			msg = 'Server didn\'t return success';

		// Check if the request actually succeeded
		} else if (response.success != false) {
			msg = 'Server returned success';

		// We should have a data attribute
		} else if (!isset(response.data)) {
			msg = 'Invalid response data';

		// Make sure we have the error
		} else 	if (!isset(response.data.ErrorCode) || !isset(response.data.ErrorReason)) {
			msg = 'No error data returned';

		} else {
			msg = response.data.ErrorReason+'<br/>code:'+response.data.ErrorCode;
		}

	} else {
		title = 'Unknown Error: ';
		msg = 'Unknown error';
	}

	Ext.Msg.show({
		title: title,
		msg: msg,
		width: 300,
		icon: Ext.MessageBox.ERROR	
	});
}





// Generic jason store
Ext.ux.JsonStore = function(config) {
	config = Ext.apply({
		url: 'ajax.php',
		remoteSort: true
	}, config);
	
	var store = new Ext.data.JsonStore(config);
	
	Ext.data.JsonStore.superclass.constructor.call(this, config);
}
Ext.extend(Ext.ux.JsonStore, Ext.data.JsonStore, {
});




// Create a generic window and specify the window, form and submission ajax configuration
Ext.ux.GenericFormWindow = function(windowConfig,formConfig,submitAjaxConfig) {

	var panelID = formConfig.formPanelID ? formConfig.formPanelID : Ext.id();
	var windowID = Ext.id();


	// Override button text?
	var submitButtonText = 'Save';
	if (submitAjaxConfig && submitAjaxConfig.submitButtonText) {
		submitButtonText = submitAjaxConfig.submitButtonText;
	}


	// Form configuration
	formConfig = Ext.apply({
		xtype: 'progressformpanel',
		id: panelID,

		// AJAX connector
		url: 'ajax.php',

		// Space stuff a bit so it looks better
		bodyStyle: 'padding: 5px',

		// Default form item is text field
		defaultType: 'textfield',

		// Button uses formBind = true, this is undocumented
		// we may need to define an event to call  'clientvalidation'
		monitorValid: true,
		
		// Buttons for the form
		buttons: [
			{
				text: submitButtonText,
				formBind: true,
				handler: function() {
					var panel = Ext.getCmp(panelID);
					var win = Ext.getCmp(windowID);

					var ajaxParams;

					if (submitAjaxConfig.params) {
						ajaxParams = submitAjaxConfig.params;

						if (submitAjaxConfig.hook) {
							var extraParams = submitAjaxConfig.hook();
						}

						ajaxParams = Ext.apply(ajaxParams,extraParams);

					} else {
						ajaxParams = submitAjaxConfig;
					}

					// Submit panel
					panel.submit({
						params: ajaxParams,
						// Close window on success
						success: function(form,action) {
							// Check if we have a custom function to execute on success
							if (submitAjaxConfig.onSuccess) {
								submitAjaxConfig.onSuccess(form,action);
							}
							win.close();
						}
					});
				}
			},{
				text: 'Cancel',
				handler: function() {
					var win = Ext.getCmp(windowID);
					win.close();
				}
			}
		],
		// Align buttons
		buttonAlign: 'center'
	}, formConfig);

	// Add any extra buttons we may have
	if (formConfig.extrabuttons) {
		// Loop and add
		for (i = 0; i < formConfig.extrabuttons.length; i += 1) {
			formConfig.buttons.push(formConfig.extrabuttons[i]);
		}
	}

	// Apply our own window configuration
	var formPanel = new Ext.ux.ProgressFormPanel(formConfig,submitAjaxConfig);
	windowConfig = Ext.apply({
		id: windowID,
		layout: 'fit',
		items: [
			formPanel
		]
	}, windowConfig);

	// Set grid panel ID
	this.formPanelID = panelID;
	this.formPanel = formPanel;

	Ext.Window.superclass.constructor.call(this, windowConfig);
}

Ext.extend(Ext.ux.GenericFormWindow, Ext.Window, {
	// Override functions here
	getFormPanel: function() {
		return this.formPanel;
	}
});




// Generic grid window
Ext.ux.GenericGridWindow = function(windowConfig,gridConfig,storeConfig,filtersConfig) {
	var panelID = Ext.id();
	var windowID = Ext.id();

	// Setup data store
	storeConfig = Ext.apply({
		autoLoad: false
	}, storeConfig);
	var store = new Ext.ux.JsonStore(storeConfig);

	// Setup filters for the grid
	var filters = new Ext.ux.grid.GridFilters(filtersConfig);

	// Setup paging toolbar
	var pagingToolbar =  new Ext.PagingToolbar({
			pageSize: 25,
			store: store,
			displayInfo: true,
			plugins: filters
	});

	// Grid configuration
	gridConfig = Ext.apply({
		xtype: 'gridpanel',
		id: panelID,
		
		plain: true,
		
		height: 300,

		// Link store
		store: store,
		
		// Plugins
		plugins: filters,
	
		// View configuration
		viewConfig: {
			forceFit: true
		},
	
		// Set row selection model
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true
		}),
		
		// Inline buttons
		buttons: [
			{
				text:'Close',
				handler: function() {
					var win = Ext.getCmp(windowID);
					win.close();
				}
			}
		],
		buttonAlign: 'center',
		
		// Bottom bar
		bbar: [
			pagingToolbar
		]
	
	}, gridConfig);

	// Store handling
	store.on('beforeload', function() { var win = Ext.getCmp(windowID); win.getEl().mask("Loading..."); } );
	store.on('load', function() { var win = Ext.getCmp(windowID); win.getEl().unmask(); } );
	store.on('exception', function(thisobj, type, action, options, response, arg) { 
			var win = Ext.getCmp(windowID); win.getEl().unmask();
			storeException(thisobj,type,action,options,response,arg);
	});

	// Apply our own window configuration
	var gridPanel = new Ext.grid.GridPanel(gridConfig);
	windowConfig = Ext.apply({
		id: windowID,
		layout: 'fit',
		items: [
			gridPanel
		]
	}, windowConfig);

	// If we have additional items, push them onto the item list
	if (windowConfig.uxItems) {
		for (i = 0; i < windowConfig.uxItems.length; i += 1) {
			windowConfig.items.push(windowConfig.uxItems[i]);
		}
	}

	// Set grid panel ID
	this.gridPanelID = panelID;
	this.gridPanel = gridPanel;

	Ext.Window.superclass.constructor.call(this, windowConfig);
}


Ext.extend(Ext.ux.GenericGridWindow, Ext.Window, {
	// Override functions here
	show: function() {
		Ext.ux.GenericGridWindow.superclass.show.call(this,arguments);

		// Load initial records
		Ext.getCmp(this.gridPanelID).store.load({
			params: {
				start: 0,
				limit: 25
			}
		});
	},

	getGridPanel: function() {
		return this.formPanel;
	}
});



function getJsonAccessor(expr) {
    var re = /[\[\.]/;
    return function(expr) {
        try {
            return(re.test(expr)) ?
            new Function("obj", "return obj." + expr) :
            function(obj){
                return obj[expr];
            };
        } catch(e){}
        return Ext.emptyFn;
    };
};

// Generic ajax request
uxAjaxRequest = function(theWindow,config) {

	var requestConfig = Ext.apply({
		url: 'ajax.php',

		// Success function
		success: function(response,options) {
			var result;

			// Try decode response, if we can't throw exception
			try {
				result = Ext.decode(response.responseText);
			} catch(e) {
				if (options.failure) {
					options.failure.call(options.scope, response, options);
				}
				if (options.callback) {
					options.callback.call(options.scope, options, false, response);
				}
				return;
			}

			// Check success property is defined
			if (!isset(result.success)) {
				if (options.failure) {
					options.failure.call(options.scope, response, options);
				}
				if (options.callback) {
					options.callback.call(options.scope, options, false, response);
				}
				return;
			}

			// Check if the request actually succeeded
			if (result.success == false) {
				var myResponse;

				// We should have a data attribute
				if (!isset(result.data)) {
					myResponse = response;

				// Make sure we have the error
				} else if (!isset(result.data.ErrorCode) || !isset(result.data.ErrorReason)) {
					myResponse = response;

				// We have everything we need
				} else {
					myResponse = result.data;
				}

				if (options.failure) {
					options.failure.call(options.scope, myResponse, options);
				}
				if (options.callback) {
					options.callback.call(options.scope, options, false, myResponse);
				}
				return;
			}

			// if we have a custom success function, run it
			if (config.customSuccess) {
				config.customSuccess(response,options);
			}
			// Lastly unmask theWindow window if we have one
			if (theWindow) {
				theWindow.getEl().unmask();
			}			
		},

		// Failure function
		failure: function(response,options) {
			var title;
			var msg;

			// Check what kind of error we got 
			if (isset(response.responseText)) {
				// Connection related
				title = "Transport Error: ";
				msg = response.responseText;

			// Backend related
			} else if (isset(response.ErrorCode) && isset(response.ErrorReason)) {
				title = "Server Error: ";
				msg = response.ErrorReason+"<br/>code:"+response.ErrorCode;
			} else {
				title = "Unknown Error";
				msg = "Unknown Error";
			}

			Ext.Msg.show({
				title: title,
				msg: msg,
				width: 300,
				icon: Ext.MessageBox.ERROR,
				fn: function() {
					if (theWindow) {
						theWindow.getEl().unmask();
					}			
				}
			});
		}
	}, config);

	Ext.Ajax.request(requestConfig);
}


// Display a check or uncheck image based on column value
function renderNiceBooleanColumn(value) {
	var displayImg = value ? 'awitef/resources/custom/images/silk/icons/cancel.png' : 
			'awitef/resources/custom/images/silk/icons/accept.png';
	return ''
		+ '<div style="text-align:center;height:13px;overflow:visible">'
		+ '<img style="vertical-align:-3px" src="' + displayImg + '" />'
		+ '</div>';
}


// vim: ts=4
