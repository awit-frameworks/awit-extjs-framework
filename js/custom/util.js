/*
JS Utils
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


/* Generate a random secure password - minimum length 8 */
function getRandomPass(len) {

	var pass = '';

	// Required keys
	var lowers = 'abcdefghjkmnpqrstuvwxyz';
	pass += lowers.charAt( Math.floor(Math.random() * lowers.length) );
	var uppers = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
	pass += uppers.charAt( Math.floor(Math.random() * uppers.length) );
	var numbers = '23456789';
	pass += numbers.charAt( Math.floor(Math.random() * numbers.length) );
	var symbols = '!@#%^*()_+=[]?';
	pass += symbols.charAt( Math.floor(Math.random() * symbols.length) );

	// Keys, combined
	var chars = lowers+uppers+numbers+symbols;

	// Fill in remaining characters
	var rem = 4;
	if (len > 8) {
		rem = len - 4;
	}
	for (i = 0; i < rem; i++) {
		pass += chars.charAt( Math.floor(Math.random() * chars.length) );
	}

	// Return shuffled string
	return shuffleString(pass);
}

/* Shuffle a string */
function shuffleString(str) {

	// String to array
	var a = str.split(""),
		n = a.length;

	// Array shuffle
	for (var i = n - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}

	// Join and return
	return a.join("");
}

/* Similar to php isset */
function isset(variable) {
	return (typeof(variable) != 'undefined');
}

// Render usage based floats
function renderUsageFloat(val) {
	return val.toFixed(2);
}

function formatUnixTimestamp(v,f) {
	if (v) {

		// Check if we have a format
		if (!f) {
			return v;
		}

		// Working with seconds
		var dt = new Date();
		dt.setTime(v * 1000);

		return dt.format(f);
	} else {
		return;
	}
}

// Return unix time, or "", or " "
function form_DateToUnix(value) {

	// Return nothing if we have no value
	if (!value) {
		return;
	}

	// Form values may be "" or " "
	var res = value;
	if (!(res == " " || res == "")) {

		// Try and parse date
		var dt = new Date.parseDate(res, 'Y-m-d H:i');
		if (!dt) {
			return;
		}

		// Convert to unix time
		var dt_unix = dt.getTime() / 1000;

		// Set result
		res = dt_unix;
	}

	return res;
}

// vim: ts=4
