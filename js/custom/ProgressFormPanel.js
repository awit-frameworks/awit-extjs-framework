

Ext.ux.ProgressFormPanel = function(config,submitAjaxConfig) {


	var submitMsg = 'Saving...';

	// Check for submit message override
	if (submitAjaxConfig && submitAjaxConfig.submitMsg) {
		submitMsg = submitAjaxConfig.submitMsg;
	}


	config = Ext.apply({
		bodyStyle: 'border:0px;',
		autoHeight: true,
		loadMsg: 'Loading...',
		submitMsg: submitMsg
	}, config);
  
	Ext.ux.ProgressFormPanel.superclass.constructor.call(this, config);

	// Create event handlers
	this.on({
		// Display failure message
    		actionfailed: { scope:this, fn:function(form, action){
			var hideMe = 0;
			var title;
			var msg;

			// We should hide the loadmask here
			if ((action.type == 'load' || action.type == 'submit') && this.rendered == true) {
				this.getLoadMask().hide();
			}

			// If we loading hide the window
			if (action.type == 'load') {
				hideMe = 1;
			}

			// Check if we have result.msg
			if (action.result && action.result.msg) {
				title = "Transport Error: ";
				msg = action.result.msg;

			// Check if we have result.errors
			} else if (action.result && action.result.errors) {
				title = "Server Error: ";

				// Check if we just have a error code
				if (action.result.data && action.result.data.ErrorCode) {
					msg = action.result.data.ErrorReason+"<br />";
					msg += "code: "+action.result.data.ErrorCode;
				// Add all errors	
				} else {
					for (var i = 0; i < action.result.errors.length; i++) {
						msg += '<br />' + action.result.errors[i];
					}
				}

			// Check if we just have a result
			} else if (
				isset(action.result) &&
				isset(action.result.data) && 
				isset(action.result.data.ErrorCode) &&
				isset(action.result.data.ErrorReason)
			) {
				title = "Server Error: ";
				msg = action.result.data.ErrorReason+"<br/>code:"+action.result.data.ErrorCode;

			// Check if we have an action response
			} else if (action.response) {
				if (action.response.status) {
					title = "Server Error: ";
				}

				msg = action.response.statusText + " (" + action.response.status + ")" ;

			// Unknown
			} else {
				title = "Unknown Error: ";
				msg = 'Unknown Error: '+ action.failureType;
			}

			// Display error
			Ext.Msg.show({
				title: title,
				msg: msg,
				icon: Ext.MessageBox.ERROR,
				modal: true,
				buttons: Ext.Msg.CANCEL,
				scope: this,
				fn: function() {
					// Check if we must hide this	
					if (hideMe) {
						this.ownerCt.hide();
					}	
				}
			});
	
		}},

    		// Before action, fire up mask
		beforeaction: { scope:this, fn:function(form, action){
			if((action.type == 'load'  || action.type == 'submit') && this.rendered == true){
				this.getLoadMask().show();
			}
		}},

		// Completed action, hide mask
		actioncomplete: { scope:this, fn:function(form, action){
			if((action.type == 'load' || action.type == 'submit') && this.rendered == true){
				this.getLoadMask().hide();
			}
		}}
	});
}

Ext.reg('progressformpanel',Ext.ux.ProgressFormPanel);

Ext.extend(Ext.ux.ProgressFormPanel, Ext.FormPanel, {
	// Setup the loadmask messages
	load: function(options){
		this.getLoadMask().msg = this.loadMsg;
		Ext.ux.ProgressFormPanel.superclass.load.call(this,options);
	},
	submit: function(options){
		this.getLoadMask().msg = this.submitMsg;
//		Ext.ux.ProgressFormPanel.superclass.submit.call(this,options);
		this.getForm().doAction('submit',options);
	},

	// Get load mask
	getLoadMask: function() {
		// If we don't have a load mask ,create one
		if(!this.loadmask){
			this.loadmask = new Ext.LoadMask(this.ownerCt.getEl());
		}
		return this.loadmask;
	}
});



